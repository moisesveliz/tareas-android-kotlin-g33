package com.example.tareas_android_kotlin_g33.sesion01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.tareas_android_kotlin_g33.databinding.ActivityTaxonomiaBinding
import java.lang.NumberFormatException

class Taxonomia : AppCompatActivity() {

    lateinit var binding: ActivityTaxonomiaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityTaxonomiaBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setup()
    }

    private fun setup() {
        binding.btnVerify.setOnClickListener {
            val strYear = binding.etYear.text.toString()
            var year: Int = 0;

            try {
                year = strYear.toInt()

                when (year) {
                    in 1994..2010 -> {
                        binding.tvResult.text = "Generacion Z"
                        binding.tvNumberPoblation.text = "7.800.00"
                        binding.tvFeatures.text = "Expansión masiva de internet"
                    }
                    in 1981..1993 -> {
                        binding.tvResult.text = "Generacion Y"
                        binding.tvNumberPoblation.text = "7.200.00"
                        binding.tvFeatures.text = "Inicio de la digitalización"
                    }
                    in 1969..1980 -> {
                        binding.tvResult.text = "Generacion X"
                        binding.tvNumberPoblation.text = "9.300.00"
                        binding.tvFeatures.text = "Crisis del 73 y transición española"
                    }
                    in 1949..1968 -> {
                        binding.tvResult.text = "Baby Boom"
                        binding.tvNumberPoblation.text = "12.200.00"
                        binding.tvFeatures.text = "Paz y explosión demográfica"
                    }
                    in 1930..1948 -> {
                        binding.tvResult.text = "Baby Boom"
                        binding.tvNumberPoblation.text = "6.300.00"
                        binding.tvFeatures.text = "Conflictos bélicos"
                    }
                    else -> {
                        binding.etYear.text.clear()
                    }
                }


            } catch (ex: NumberFormatException) {
                Log.e("Taxonomia", "Error al ingresar número")
                binding.etYear.text.clear()
            }
        }
    }
}