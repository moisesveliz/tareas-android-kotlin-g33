package com.example.tareas_android_kotlin_g33.sesion02

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.RadioButton
import com.example.tareas_android_kotlin_g33.databinding.ActivityMascotaFormBinding
import android.widget.CheckBox


class MascotaForm : AppCompatActivity() {

    lateinit var binding: ActivityMascotaFormBinding
    private var vaccines: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMascotaFormBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setup()
    }

    private fun setup() {

        binding.cbVacinne1.setOnClickListener { selectedVaccine(it as CheckBox) }
        binding.cbVaccine2.setOnClickListener { selectedVaccine(it as CheckBox) }
        binding.cbVaccine3.setOnClickListener { selectedVaccine(it as CheckBox) }
        binding.cbVaccine4.setOnClickListener { selectedVaccine(it as CheckBox) }
        binding.cbVaccine5.setOnClickListener { selectedVaccine(it as CheckBox) }

        binding.btnSend.setOnClickListener {
            val name = binding.tvName.text.toString()
            val age = binding.tvAge.text.toString()

            val checkedPet: Int? = binding.rbgPets.checkedRadioButtonId
            val pet = checkedPet.takeIf { it != -1 }?.let { findViewById<RadioButton>(it).text.toString() } ?: "No Seleccionó"


            val bundle = Bundle().apply {
                putString("KEY_NAME", name)
                putString("KEY_AGE", age)
                putString("KEY_PET", pet)
                putString("KEY_VACCINES", vaccines.toString())
            }

            val intent = Intent(this, ResultadoMascota::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }

    private fun selectedVaccine(view: CheckBox) {
        val checked = view.isChecked
        if (checked) {
            vaccines.add(view.text.toString())
        } else {
            vaccines.remove(view.text.toString())
        }
    }

}