package com.example.tareas_android_kotlin_g33.sesion02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.tareas_android_kotlin_g33.R

class ResultadoMascota : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado_mascota)

        val bundle : Bundle?  = intent.extras

        val name = bundle?.getString("KEY_NAME","Desconocido") ?: "Desconocido"
        val age = bundle?.getString("KEY_AGE","Desconocido") ?: "Desconocido"
        val pet = bundle?.getString("KEY_PET","Desconocido") ?: "Desconocido"
        val vaccines = bundle?.getString("KEY_VACCINES","Desconocido") ?: "Desconocido"

        findViewById<TextView>(R.id.tvResultName).text = name
        findViewById<TextView>(R.id.tvResultAge).text = age
        findViewById<TextView>(R.id.tvResultPet).text = pet
        findViewById<TextView>(R.id.tvResultVaccines).text = vaccines
    }
}